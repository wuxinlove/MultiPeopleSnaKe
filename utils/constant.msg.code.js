/**
 * 通用值设置
 * @type {{OK: number, NO: number}}
 */
module.exports = {
  "OK": 1,
  "ERR": 0
}
