;
(function () {
  function Grid() {
    this.ctx = document.getElementById(arguments[0]).getContext('2d');
    this.audio = document.getElementById(arguments[1]);
    this.socketId = arguments[2].id;
    this.init();
  }

  Grid.prototype.init = function () {
    return this.initParam().drawText().drawGrid().initGrid()
  }

  Grid.prototype.initParam = function () {
    this.span = 50;
    this.height = this.ctx.canvas.height;
    this.width = this.ctx.canvas.width - 200;
    this.perHeight = this.height / this.span;
    this.perWidth = this.width / this.span;
    return this;
  }

  Grid.prototype.drawText = function () {
    this.ctx.fillStyle = 'red';
    //clearRect(this.width+20,60,200,20);
    //this.ctx.fillText('多人在线贪吃蛇蛇游戏哈哈哈哈哈', this.width + 20, 60);
    this.ctx.fillText('多人在线贪吃蛇蛇游戏', this.width + 20, 60);
    this.ctx.fillText('↑↓←→控制蛇的方向', this.width + 20, 80);
    this.ctx.fillText('空格复活', this.width + 20, 100);
    return this;
  }

  Grid.prototype.showTip = function () {
    var me = this;
    if (!this.tipSettimeoutId) {
      this.tipSettimeoutId = setTimeout(function () {
        me.ctx.fillStyle = 'red';
        me.ctx.clearRect(me.width + 20, 120, 200, 25);
        me.ctx.fillText(me.msg.shift(), me.width + 20, 140);
        if (!me.msg.length) {
          clearTimeout(me.tipSettimeoutId);
          me.tipSettimeoutId = 0;
        }
      }, 1000);
    }
  }

  Grid.prototype.pushMsg = function (msg) {
    (this.msg = (this.msg || [])).push(msg.replace(this.socketId, '您'));
    this.showTip();
    return this;
  }

  Grid.prototype.playAudio = function () {
    if (this.audio && this.audio.play) {
      this.audio.play();
    }
    return this;
  }

  Grid.prototype.drawGrid = function () {
    var me = this;
    this.ctx.strokeStyle = 'black';
    this.ctx.lineWidth = 1;
    for (var i = 0; i <= this.span; i++) {
      this.ctx.moveTo(i * this.perWidth, 0);
      this.ctx.lineTo(i * this.perWidth, this.height);
      this.ctx.moveTo(0, i * this.perHeight);
      this.ctx.lineTo(this.width, i * this.perHeight);
    }
    this.ctx.stroke();
    return this;
  }

  Grid.prototype.initGrid = function () {
    this.web = [];
    for (var i = 0; i < this.span; i++) {
      this.web[i] = [];
      for (var j = 0; j < this.span; j++) {
        this.web[i][j] = 0;// Math.round(Math.random());
      }
    }
    return this;
  }

  Grid.prototype.draw = function () {
    var me = this, x, y;
    for (var i = this.web.length - 1; i > -1; i--) {
      x = i * this.perWidth;
      for (var j = this.web[i].length - 1; j > -1; j--) {
        y = j * this.perHeight;
        if (this.web[i][j] === 10 || this.web[i][j] === 12) {// myself or warm
          this.ctx.fillStyle = 'red';
        } else if (this.web[i][j] === 13) {// mushroom
          var img = document.createElement('img');
          console.log(x, y);
          img.onload = (function (x, y) {
            return function () {
              me.ctx.drawImage(this, 0, 0, this.width, this.height, x, y, me.perWidth, me.perHeight);
            }
          })(x, y);
          img.src = '/public/img/mushroom.jpg';
          continue;
        } else if (this.web[i][j] === 11) {// other gamer
          this.ctx.fillStyle = 'blue';
        } else {// dead
          if (this.web[i][j] === 20) {
            this.playAudio();
          }
          this.ctx.fillStyle = 'white';
        }
        this.ctx.beginPath();
        this.ctx.rect(x, y, this.perWidth, this.perHeight);
        this.ctx.fill();
        this.ctx.stroke();
      }
    }
    return this;
  }

  Grid.prototype.receive = function (snakes) {
    if (snakes) {
      this.initGrid();
      for (var i = snakes.length - 1; i > -1; i--) {
        for (var j = snakes[i].data.length - 1; j > -1; j--) {
          if (snakes[i].status) {// live
            if (snakes[i].id === this.socketId) {// myself
              this.web[snakes[i].data[j].x][snakes[i].data[j].y] = 10;
            } else if (snakes[i].status === 'fixed') {// warm
              if (snakes[i].type && snakes[i].type === 'mushroom') {
                this.web[snakes[i].data[j].x][snakes[i].data[j].y] = 13;
              } else {
                this.web[snakes[i].data[j].x][snakes[i].data[j].y] = 12;
              }
            } else {// other gamer
              this.web[snakes[i].data[j].x][snakes[i].data[j].y] = 11;
            }
          } else {// dead
            this.web[snakes[i].data[j].x][snakes[i].data[j].y] = 20;
          }
        }
      }
    }
    return this.draw();
  }

  window.Grid = Grid;
})();


;
$(function () {
  var keys = {'37': 'left', '38': 'up', '39': 'right', '40': 'down', '32': 'revive'};
  var socket = io.connect();
  socket.on('connect', function () {
    var grid = new Grid('canvas', 'bong', socket);
    socket.on('message', function (msg) {
      grid.receive(msg.snakes);
    }).on('disconnect', function () {
    }).on('tip', function (msg) {
      grid.pushMsg(msg);
    });
  });
  $(window).on('keydown', function (e) {
    if (keys[e.which]) {
      socket.send(keys[e.which]);
    }
  });
});